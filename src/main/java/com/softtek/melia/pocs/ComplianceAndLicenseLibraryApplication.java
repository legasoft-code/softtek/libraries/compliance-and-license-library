package com.softtek.melia.pocs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComplianceAndLicenseLibraryApplication {
    public static void main(String[] args) {
        SpringApplication.run(ComplianceAndLicenseLibraryApplication.class, args);
    }
}